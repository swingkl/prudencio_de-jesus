﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TowerArrow : MonoBehaviour
{
    //public float Speed = 10;
    //public float rotationSpeed = 10f;
    public GameObject Arrow;
    public List<GameObject> enemiesInRange;
    private Transform myStart;
    private Transform myEnemyTarget;

    private Vector3 targetPoint;
    private Vector3 point;
    private Quaternion targetRotation;
    int arrowSpwn = 5;

    private bool trigg = false;
    // Use this for initialization


    void Awake()
    {
        myStart = transform;
    }
    void Start ()
    {
        myEnemyTarget = GameObject.FindWithTag("Enemy").transform;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (trigg == true)
        {

            point = myEnemyTarget.position;
            point.y = transform.position.y;
        }
        else if (trigg == false)
        {
            StopAllCoroutines();
        }
    }

    void OnTriggerStay(Collider col)
    {
        if (col.tag == "Enemy")
        {
            ///Look for enemy///
            transform.LookAt(point);
            trigg = true;
            //myEnemyTarget = col.gameObject.transform;
            StartCoroutine(Shoot(col.gameObject));
            //myStart.rotation = Quaternion.Slerp(myStart.rotation, Quaternion.LookRotation(myEnemyTarget.position - myStart.position), rotationSpeed * Time.deltaTime);
        }
    }

    void OnTriggerExit(Collider col)
    {
        trigg = false;
    }


 
    IEnumerator Shoot(GameObject target)
    {

            yield return new WaitForSeconds(1);
            Vector3 pos = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
            GameObject arrowClone = (GameObject) Instantiate(Arrow, pos, Quaternion.identity);
            arrowClone.GetComponent<ArrowSpd>().myEnemyTarget = target.transform;
        yield return new WaitForSeconds(3);
       // }
        yield return 2;
        //}


    }
}
