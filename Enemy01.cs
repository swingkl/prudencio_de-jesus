﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Enemy01 : MonoBehaviour
{
    private NavMeshAgent agent;
    public GameObject Destination;
    private int EnemySpd = 5;
    int StartHP = 10;
    public int CurrentHP;
    public Slider HPSlider;
    bool damaged;
    bool isDead;

    public static int ArrowDamage = 1;
    public int IceDamage = 1;
    public int FireDamage = 2;
    public int NormCannDamage = 3;
    public int Speed = 5;
    // Use this for initialization
    void Start()
    {
        CurrentHP = StartHP;
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        damaged = false;
        agent.speed = 1 * Speed;
        agent.SetDestination(Destination.transform.position);
    }

    public void TakeDamage(int amount)
    {
        damaged = true;

        CurrentHP -= amount;

        HPSlider.value = CurrentHP;

        if (CurrentHP <= 0 & !isDead)
        {
            Destroy(this.gameObject);
        }
    }

    void OnTriggerEnter(Collider Trig)
    {
        if (Trig.gameObject.tag == "Arrow")
        {
            Debug.Log("PIerccce!");
            this.TakeDamage(ArrowDamage);
            Destroy(Trig.gameObject);
        }

        if(Trig.gameObject.tag == "Finish")
        {
            Destroy(this.gameObject);
        }

        if (Trig.gameObject.tag == "Norm")
        {
            this.TakeDamage(NormCannDamage);
            Destroy(Trig.gameObject);
        }

        if (Trig.gameObject.tag == "Ice")
        {
            this.TakeDamage(IceDamage);
            Destroy(Trig.gameObject);
            
        }

        if (Trig.gameObject.tag == "Fire")
        {
            this.TakeDamage(FireDamage);
            Destroy(Trig.gameObject);
        }
    }
}
