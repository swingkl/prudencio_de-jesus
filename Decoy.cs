﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Decoy : MonoBehaviour
{
    public int StartHP = 5;
    public int CurrentHP;
    public Slider HPSlider;
    bool damaged;
    bool isDead;
    public int AtkDamage = 1;
    //private Transform shot;
    // Use this for initialization

    void Awake()
    {
        CurrentHP = StartHP;

    }
	void Start ()
    {
        //shot = GameObject.FindWithTag("Arrow").transform;
    }
	
	// Update is called once per frame
	void Update ()
    {
        damaged = false;
    }

    public void TakeDamage (int amount)
    {
        damaged = true;

        CurrentHP -= amount;

        HPSlider.value = CurrentHP;

        if(CurrentHP <= 0 & !isDead)
        {
            Destroy(this.gameObject);
        }
    }

    void OnTriggerEnter(Collider Trig)
    {
        if (Trig.gameObject.tag == "Arrow")
        {
            Debug.Log("PIerccce!");
            this.TakeDamage(AtkDamage);
            Destroy(Trig.gameObject);
            //GetComponent<TowerArrow>().trigg = false;
        }

        if (Trig.gameObject.tag == "Ice")
        {
            Debug.Log("FREEEZ!");
            Destroy(Trig.gameObject);
        }

        if (Trig.gameObject.tag == "Fire")
        {
            Debug.Log("BURRRN!");
            Destroy(Trig.gameObject);
        }

        if (Trig.gameObject.tag == "Norm")
        {
            Debug.Log("Bluntt!");
            Destroy(Trig.gameObject);
        }
    }
}
