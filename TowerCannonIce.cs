﻿using UnityEngine;
using System.Collections;

public class TowerCannonIce : MonoBehaviour
{
    public float Speed = 10;
    public GameObject Cannon;
    public float rotationSpeed = 10f;

    public float xx = 0;
    public float yy = 0;
    public float zz = 0;

    private Transform myStart;
    private Transform myEnemyTarget;

    private Vector3 targetPoint;
    private Vector3 point;
    private Quaternion targetRotation;
    int CannonSpwn = 5;
    // Use this for initialization
    void Awake()
    {
        myStart = transform;
        //InvokeRepeating("Shoot", 0, 10);
    }
    void Start()
    {
        myEnemyTarget = GameObject.FindWithTag("Enemy").transform;
    }

    // Update is called once per frame
    void Update()
    {
        point = myEnemyTarget.position;
        point.y = transform.position.y;
    }

    void OnTriggerStay(Collider col)
    {
        if (col.tag == "Enemy")
        {

            ///Look for enemy///
            transform.LookAt(point);

            ///Starts shooting when see an enemy///
            StartCoroutine(Shoot());

            //myEnemyTarget = col.gameObject.transform;
            //Shoot();
            //myStart.rotation = Quaternion.Slerp(myStart.rotation, Quaternion.LookRotation(myEnemyTarget.position - myStart.position), rotationSpeed * Time.deltaTime);
        }

    }

    IEnumerator Shoot()
    {

        // while(true)
        //{
        int CurrentEnemies = 0;
        for (int i = CurrentEnemies; i < CannonSpwn; i++)
        {
            yield return new WaitForSeconds(1);
            Vector3 pos = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
            Instantiate(Cannon, pos, Quaternion.identity);
        }
        yield return 0;
        //}


    }
}