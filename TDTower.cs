﻿using UnityEngine;
using System.Collections;

public class TDTower : MonoBehaviour {
    public GameObject ArrowTower;
    public GameObject Cannon;
    public GameObject FireCannon;
    public GameObject IceTower;
    public bool one,two,three,four = false;
	// Use this for initialization
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            one = true;
            two = false;
            three = false;
            four = false;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            one = false;
            two = true;
            three = false;
            four = false;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            one = false;
            two = false;
            three = true;
            four = false;
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            one = false;
            two = false;
            three = false;
            four = true;
        }
    }
    void OnMouseDown () 
    {
 
        if (one == true)
        {
            Vector3 position = new Vector3(this.transform.position.x, this.transform.position.y - 1, this.transform.position.z);
            Instantiate(ArrowTower, position, Quaternion.identity);
            Destroy(this.gameObject);

        }
        if (two == true)
        {
            Vector3 position = new Vector3(this.transform.position.x, this.transform.position.y-1, this.transform.position.z);
            Instantiate(Cannon, position, Quaternion.identity);
          Destroy(this.gameObject);
        }
        if (three == true)
        {
            Vector3 position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
            Instantiate(FireCannon, position, Quaternion.identity);
            Destroy(this.gameObject);
        }
        if (four == true)
        {
            Vector3 position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
            Instantiate(IceTower, position, Quaternion.identity);
            Destroy(this.gameObject);
        }
    }
}
