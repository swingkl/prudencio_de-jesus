﻿using UnityEngine;
using System.Collections;

public class ArrowSpd : MonoBehaviour
{
    private Transform myStart;
   public Transform myEnemyTarget;

    //private Transform myEnemyRotX;
    //private Transform myEnemyRotY;
    //private Transform myEnemyRotZ;

    public float speed = 15.0f;
    public float rotationSpeed = 10f;
    //public int AtkDamage = 1;
    //Decoy Hp;
    // Use this for initialization
    void Awake()
    {
        myStart = transform;
    }
    void Start()
    {
        //this.transform.Rotate(Vector3.right * Time.deltaTime); arrowClone.GetComponent<ArrowSpd>().myEnemyTarget = GameObject.FindWithTag("Enemy").transform;
    }
    // Update is called once per frame
    void Update()
    {
        if (myEnemyTarget != null)
        {

            myStart.rotation = Quaternion.Slerp(myStart.rotation, Quaternion.LookRotation(myEnemyTarget.position - myStart.position), rotationSpeed * Time.deltaTime);
            //move towards the player
            myStart.position += myStart.forward * speed * Time.deltaTime;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    //void Atk()
    //{
    //    if (Hp.CurrentHP > 0)
    //    {
    //        Hp.TakeDamage(AtkDamage);
    //    }
    //}



}
