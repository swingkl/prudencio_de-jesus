﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Wave
{
    public GameObject enemyPrefab;
    public float spawnInterval = 2;
    public int maxEnemies = 20;
}
public class PortalSpawner : MonoBehaviour {
    public Wave[] waves;
    public int timeBetweenWaves = 5;
    private GamManager gamManager;

    private float spawnTime;
    private int enemiesSpawned = 0;
    int EnemySpwn = 20;
    public GameObject Enemy01;

    // Use this for initialization
    void Start()
    {
        spawnTime = Time.time;
        gamManager = GameObject.Find("GamManager").GetComponent<GamManager>();
      // StartCoroutine(Spawn());
    }

    // Update is called once per frame
    void Update()
    {
        int currentWave = gamManager.Wave;
        if (currentWave < waves.Length)
        {
            // 2
            float timeInterval = Time.time - spawnTime;
            float spawnInterval = waves[currentWave].spawnInterval;
            if (((enemiesSpawned == 0 && timeInterval > timeBetweenWaves) ||
                 timeInterval > spawnInterval) &&
                enemiesSpawned < waves[currentWave].maxEnemies)
            {
                // 3  
                spawnTime = Time.time;
                GameObject newEnemy = (GameObject)
                    Instantiate(waves[currentWave].enemyPrefab);
                //newEnemy.GetComponent<EnemyWP>().waypoints = waypoints;
                enemiesSpawned++;
            }
            // 4 
            if (enemiesSpawned == waves[currentWave].maxEnemies &&
                GameObject.FindGameObjectWithTag("Enemy") == null)
            {
                gamManager.Wave++;
                gamManager.Gold = Mathf.RoundToInt(gamManager.Gold * 1.1f);
                enemiesSpawned = 0;
                spawnTime = Time.time;
            }
            // 5 
        }
        else
        {
            gamManager.gameOver = true;
            GameObject gameOverText = GameObject.FindGameObjectWithTag("GameWon");
            gameOverText.GetComponent<Animator>().SetBool("gameOver", true);
        }
    }

}